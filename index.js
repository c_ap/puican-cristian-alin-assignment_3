
const FIRST_NAME = "Cristian-Alin";
const LAST_NAME = "Puican";
const GRUPA = "1092";

/**
 * Make the implementation here
 */
class Employee {
    constructor(name, surname, salary){
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }

    getDetails(){
        return this.name+" "+this.surname+" "+this.salary;
    }
}

class SoftwareEngineer extends Employee {
    constructor(name, surname, salary, experience = "JUNIOR"){
        super(name,surname,salary);
        this.experience = experience;
    }

    applyBonus(){
        switch (this.experience){
            case "JUNIOR":
                return this.salary*1.10;
                break;

            case "MIDDLE":
                return this.salary*1.15;
                break;
            
            case "SENIOR":
                return this.salary*1.20;
                break;

            default:
                return this.salary*1.10;
                break;
        }
    };
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

